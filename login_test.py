from selenium import webdriver
import pytest
import os

class TestGloboplay():

    @pytest.fixture()
    def test_setup(self):
        path_chromedriver = './chromedriver'
        self.driver = webdriver.Chrome(executable_path=path_chromedriver)
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()
        yield
        self.driver.close()

    @pytest.mark.funcional
    def test_login_fail(self, test_setup):
        self.driver.get("https://globoplay.globo.com")

        #self.close_msg_cookies()
        self.driver.save_screenshot('./screenshot_test_login_fail.png')
        assert self.driver.title == 'Globoplay | Assista online aos programas da Globo'



